import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priceFormat'
})
export class PriceFormatPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if(!value) return null;
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

}
