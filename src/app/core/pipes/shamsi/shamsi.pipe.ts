import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';

@Pipe({
  name: 'shamsi'
})
export class ShamsiPipe implements PipeTransform {
  constructor(
    @Inject(LOCALE_ID) public locale: string,
  ){}

  transform(value: any, ...args: any[]): unknown {
    if(!value) return null;
    const options = {  year: 'numeric', month: 'long', day: 'numeric' };
    if (args && args[0] === 'dt') {
      return new Date(value).toLocaleDateString(this.locale,options) + ' - ' + 
      new Date(value).toLocaleTimeString(this.locale, { hour: '2-digit', minute: '2-digit' });
    } else {
      return new Date(value).toLocaleDateString(this.locale,options)
    }
  }

}
