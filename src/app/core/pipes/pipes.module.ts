import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShamsiPipe } from './shamsi/shamsi.pipe';
import { SanitizerHtmlPipe } from './sanitizer-html/sanitizer-html.pipe';
import { PriceFormatPipe } from './price-format/price-format.pipe';



@NgModule({
  declarations: [
    ShamsiPipe,
    SanitizerHtmlPipe,
    PriceFormatPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ShamsiPipe,
  ]
})
export class PipesModule { }
