import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class DataService implements OnInit {

  // variable for set params in url
  private count: number;

  private urlFormat(url: string, params: any): any {
    this.count = 0;
    params.forEach(key =>  {
      url = url.replace( ('{' + this.count++ + '}') , key);
    });
    return url;
  }
  constructor(
    private http: HttpClient,
    private errorHandler: ErrorHandlerService) { }

  ngOnInit(){

  }

  // get method
  get(url: string, params?: Array<any>): Observable<any> {
    if (params) {
      url = this.urlFormat(url, params);
    }

    return this.http.get(environment.url + url )
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }

  /// post method
  post(url: string, data?: any, params?: Array<any>): Observable<any> {
    if (params) {
      url = this.urlFormat(url, params);
    }

    return this.http.post(environment.url + url, data)
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }

  // delete method
  delete(url: string, params?: Array<any>): Observable<Response> {
    if (params) {
      url = this.urlFormat(url, params);
    }

    return this.http.delete(environment.url + url )
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      )
  }

  // put method
  put(url: string, data?: any, params?: Array<any>): Observable<Response> {
    if (params) {
      url = this.urlFormat(url, params);
    }

    return this.http.put(environment.url + url, data)
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(this.handelError)
      );
  }


  handelError(error: any): any {
    return this.errorHandler.handel(error);
  }
}
