import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  // set token
  public setToken(value: string): void {
    localStorage.setItem('token',value);
  }

  // get token
  public getToken(): string {
    return localStorage.getItem('token');
  }

  // isAuthenticated()

}
