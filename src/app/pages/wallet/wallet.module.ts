import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './wallet-routing.module';
import { WalletComponent } from './wallet.component';
// import { HeaderComponent } from '../shared/components/header/header.component';
// import { TopHeaderComponent } from '../shared/components/header/top-header/top-header.component';
// import { HamburgerMenuComponent } from '../shared/components/header/hamburger-menu/hamburger-menu.component';
// import { FooterComponent } from '../shared/components/footer/footer.component';



@NgModule({
  declarations: [
    WalletComponent,
    // HeaderComponent,
    // TopHeaderComponent,
    // HamburgerMenuComponent,
    // FooterComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
  ]
})
export class WalletModule { }
