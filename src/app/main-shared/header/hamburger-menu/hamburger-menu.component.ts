import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.less']
})
export class HamburgerMenuComponent implements OnInit {

  menuStatus: Boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu(value: Boolean): void{
    this.menuStatus = value
  }

}
