import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TopHeaderComponent } from './header/top-header/top-header.component';
import { HamburgerMenuComponent } from './header/hamburger-menu/hamburger-menu.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    TopHeaderComponent,
    HamburgerMenuComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent
  ]
})
export class MainSharedModule { }
